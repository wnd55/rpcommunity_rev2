<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use  frontend\widgets\grid\MetersDataDetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MetersData */
/** @var  $counters */
$this->title = 'Показание воды';
$this->params['breadcrumbs'][] = ['label' => 'Показание воды', 'url' => 'index'];
$this->params['breadcrumbs'][] = $this->title;

$visible = null;
if ($counters === true) {
    $visible = false;
} else {

    $visible = true;
}

?>
<div>


    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->idmetersdata], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->idmetersdata], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Переслать на email', ['/meters-data/meters-data-email', 'id' => $model->idmetersdata, 'self' => true], [
            'class' => 'btn btn-success',]) ?>

        <?= Html::a('Переслать в ТСЖ', ['/meters-data/meters-data-email', 'id' => $model->idmetersdata, 'self' => false], [
            'class' => 'btn btn-warning',]) ?>
    </p>
    <?= MetersDataDetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'watermeter_id',
                'contentOptions' => ['style' => 'background-color:#0027ff14'],
            ],
            'cold1',
            [
                'attribute' => 'wmcold2',
                'contentOptions' => ['style' => 'background-color:#0027ff14'],
            ],
            'cold2',
            [
                    'attribute' => 'wmcold3',
                'contentOptions' => ['style' => 'background-color:#0027ff14'],
                'visible' => $visible
            ],
            'cold3',
            [
                'attribute' => 'wmhot1',
                'contentOptions' => ['style' => 'background-color:#FFA39229'],
            ],
            'hot1',
            [
                'attribute' => 'wmhot2',
                'contentOptions' => ['style' => 'background-color:#FFA39229'],
            ],
            'hot2',
            [
                'attribute' => 'wmhot3',
                'contentOptions' => ['style' => 'background-color:#FFA39229'],
            ],
            'hot3',
            [
                'attribute' => 'date',
                'format' => ['datetime', 'php:d.m.Y H:i:s']
            ],
        ]
    ])
    ?>

</div>

