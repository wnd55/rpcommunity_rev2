<?php
return [
    'adminEmail' => 'webndesign55@gmail.com',
    'supportEmail' => 'webndesign55@gmail.com',
    'tsjEmail' => 'ruble-buh@yandex.ru',
    'senderEmail' => 'noreply@rpcommunity.ru',
    'senderName' => 'rpcommunity.ru mailer',
    'user.passwordResetTokenExpire' => 3600,
];
